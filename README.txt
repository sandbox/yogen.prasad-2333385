MODULE NAME
-------------------------------------------------------------------------------
OG membership subscription


DESCRIPTION AND USAGE
-------------------------------------------------------------------------------

Download module and put it at sits/all/modules/*

Install module

After installing this module you will get a
field name : Privacy settings(field_og_subscribe_settings) in Og Content types

Note: If the Og Content type is not created at the installation time then after
creating the content type revert the feature so that the field must be appear
for the new content type.

This module provides a field level settings for making your OG Content type as


a.) Open

b.) Moderated

c.) Closed

d.) Invite Only

The Group privacy settings can be done by editing the OG.

There is a admin settings to set the purmissions according to the privacy
setiings at: admin/config/membership-type

Here you have to set content type(machine name) for which this feature needs
to be work

Then You have to set the permissions for the corresponding privacy settings




CONFIGURATION
-------------------------------------------------------------------------------
You can configure module settings at :
    'admin/config/membership-type'

Here you will find a text-field where you have to enter your OG Content
type's machine name ex(group)

Here you will find menu tabs naming : Closed,Invite,Moderated,Open

These tabs have there own forms and settings for the permissions
corresponding to the values

You have to save the desired values

and then You can create contents and the subscription permissions
will work accordingly


NOTE: Currently This module works only for single OG Content type.


FUTURE RELEASES
-------------------------------------------------------------------------------
The future releases of this module will provide :
- Working with more than one Organic Group Content Type.


CREDITS
-------------------------------------------------------------------------------
Yogendra Prasad < yogen.prasad@gmail.com >
D.O. username < yogen.prasad >

-------------------------------------------------------------------------------
