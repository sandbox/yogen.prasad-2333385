<?php

/**
 * @file
 * File ioc_groups.features.field_instance.inc.
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ioc_groups_field_default_field_instances() {
  $field_instances = array();
  $node_type = og_get_all_group_bundle();
  foreach ($node_type as $value) {
    foreach ($value as $type_name) {
      $field_instances['node-group-field_og_subscribe_settings'] = array(
        'bundle' => $type_name,
        'default_value' => array(
          0 => array(
            'value' => 'open',
          ),
        ),
        'deleted' => 0,
        'description' => 'These privacy settings will not change the visibility of content that has already posted into this group. It takes effect only for content created after the group is saved with new privacy settings.',
        'display' => array(
          'default' => array(
            'label' => 'above',
            'module' => 'list',
            'settings' => array(),
            'type' => 'list_default',
            'weight' => 4,
          ),
          'notifications' => array(
            'label' => 'above',
            'settings' => array(),
            'type' => 'hidden',
            'weight' => 0,
          ),
          'teaser' => array(
            'label' => 'above',
            'settings' => array(),
            'type' => 'hidden',
            'weight' => 0,
          ),
        ),
        'entity_type' => 'node',
        'field_name' => 'field_og_subscribe_settings',
        'label' => 'Privacy settings',
        'required' => 1,
        'settings' => array(
          'user_register_form' => FALSE,
        ),
        'widget' => array(
          'active' => 1,
          'module' => 'options',
          'settings' => array(),
          'type' => 'options_buttons',
          'weight' => 41,
        ),
      );

      t('Privacy settings');
      t('These privacy settings will not change the visibility of content that has already posted into this group. It takes effect only for content created after the group is saved with new privacy settings.');
    }
  }
  // Exported field_instance: 'node-community-field_og_subscribe_settings'
  return $field_instances;
}
