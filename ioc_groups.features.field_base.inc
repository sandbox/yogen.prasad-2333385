<?php

/**
 * @file
 * File ioc_groups.features.field_base.inc.
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ioc_groups_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_og_subscribe_settings'
  $field_bases['field_og_subscribe_settings'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_og_subscribe_settings',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'open' => 'Open - Membership requests are accepted immediately.',
        'moderated' => 'Moderated - Joining requires admin approval',
        'invite' => 'Invite Only - Joining requires an invitation.',
        'closed' => 'Closed - Membership is exclusively managed by an administrator',
      ),
      'allowed_values_function' => '',
      'profile2_private' => FALSE,
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
