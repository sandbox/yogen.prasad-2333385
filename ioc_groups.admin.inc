<?php

/**
 * @file
 * File for providing admin settings.
 */

/**
 * Implement page call back for admin setting form.
 */
function ioc_groups_settings_page() {
  $form["ioc_groups_type_content_name"] = array(
    "#type" => "textfield",
    "#title" => t("OG Content type machine name"),
    "#default_value" => variable_get("ioc_groups_type_content_name", NULL),
    "#description" => t("Set OG Content type machine name so that the roles can be used"),
  );
  // Send our form to Drupal to make a settings page.
  return system_settings_form($form);
}

/**
 * Menu callback: administer permissions.
 */
function ioc_groups_permissions($form, $form_state, $type) {

  module_load_include('inc', 'og_ui', 'og_ui.admin');

  $role_names = og_roles('node', variable_get("ioc_groups_type_content_name", "group"));
  // Fetch permissions for all roles or the one selected role.
  $assiend_roles = ioc_groups_get_default_values_for_checkboxes($role_names, $type);
  // Store $role_names for use when saving the data.
  $form['role_names'] = array(
    '#type' => 'value',
    '#value' => $role_names,
  );
  // Render role/permission overview:
  $options = array();
  $module_info = system_get_info('module');
  // Get a list of all the modules implementing a hook_permission() and sort by
  // display name.
  $permissions_by_module = array();

  foreach (og_get_permissions() as $perm => $value) {
    if ($value['module'] == "og_ui" || $value['module'] == "og_invite_people") {
      $module = $value['module'];
      $permissions_by_module[$module][$perm] = $value;
    }
  }

  foreach ($permissions_by_module as $module => $permissions) {
    $form['permission'][] = array(
      '#markup' => $module_info[$module]['name'],
      '#id' => $module,
    );

    foreach ($permissions as $perm => $perm_item) {
      // Fill in default values for the permission.
      $perm_item += array(
        'description' => '',
        'restrict access' => FALSE,
        'warning' => !empty($perm_item['restrict access']) ? t('Warning: Give to trusted roles only; this permission has security implications in the group context.') : '',
      );
      // If the user can manage permissions, but does not have administer
      // group permission, hide restricted permissions from them. This
      // prevents users from escalating their privileges.
      $options[$perm] = '';
      $form['permission'][$perm] = array(
        '#type' => 'item',
        '#markup' => $perm_item['title'],
        '#description' => theme('user_permission_description', array('permission_item' => $perm_item)),
      );
      foreach ($role_names as $rid => $name) {
        // Builds arrays for checked boxes for each role.
        if (isset($assiend_roles[$rid][$perm])) {
          $status[$rid][] = $perm;
        }
      }
    }
  }
  foreach ($role_names as $rid => $name) {
    $form['checkboxes'][$rid] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[$rid]) ? $status[$rid] : array(),
      '#attributes' => array('class' => array('rid-' . $rid)),
    );
    $form['role_names'][$rid] = array('#markup' => check_plain($name), '#tree' => TRUE);
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save permissions'),
    '#submit' => array('ioc_groups_membership_submit'),
  );

  $form['membership_type'] = array(
    "#type" => "hidden",
    "#value" => $type,
  );
  return $form;
}

/**
 * Theme the administer permissions page.
 *
 * @ingroup themeable
 */
function theme_ioc_groups_permissions($variables) {
  $form = $variables['form'];
  $role_names = $form['role_names']['#value'];
  foreach (element_children($form['permission']) as $key) {
    $row = array();
    // Module name.
    if (is_numeric($key)) {
      $row[] = array(
        'data' => drupal_render($form['permission'][$key]),
        'class' => array('module'),
        'id' => 'module-' . $form['permission'][$key]['#id'],
        'colspan' => count($form['role_names']['#value']) + 1);
    }
    else {
      $row[] = array(
        'data' => drupal_render($form['permission'][$key]),
        'class' => array('permission'),
      );
      foreach (element_children($form['checkboxes']) as $rid) {
        $form['checkboxes'][$rid][$key]['#title'] = $role_names[$rid] . ': ' . $form['permission'][$key]['#markup'];
        $form['checkboxes'][$rid][$key]['#title_display'] = 'invisible';
        $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'class' => array('checkbox'));
      }
    }
    $rows[] = $row;
  }
  $header[] = (t('Permission'));
  foreach (element_children($form['role_names']) as $rid) {
    $header[] = array('data' => drupal_render($form['role_names'][$rid]), 'class' => array('checkbox'));
  }
  $output = '';
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('id' => 'permissions')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * Implements form submit.
 */
function ioc_groups_membership_submit($form, &$form_state) {
  foreach ($form_state['values']['role_names'] as $rid => $name) {
    ioc_groups_change_permissions($rid, $form_state['values']['membership_type'], $form_state['values'][$rid]);
  }
}

/**
 * Implements function to get Default Values.
 */
function ioc_groups_get_default_values_for_checkboxes($role_names, $type) {
  $results = array();
  foreach ($role_names as $key => $value) {
    $result = db_select('ioc_groups_membership_permission', 'og')
        ->fields('og', array('permission'))
        ->condition('membership', $type)
        ->condition('rid', $key)
        ->execute()
        ->fetchAll();
    foreach ($result as $values) {
      $results[$key][$values->permission] = $values->permission;
    }
  }
  return $results;
}
